﻿using System;

namespace Data.Domain
{
    public class BaseEntity
    {
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }

        public int CreateUserId { get; set; }

        public DateTime? EditDate { get; set; }

        public int? EditUserId { get; set; }

        public DateTime? DeleteDate { get; set; }

        public int? DeleteUserId { get; set; }
    }
}
