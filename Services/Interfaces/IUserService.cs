﻿using Data.Domain;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IUserService
    {
        List<User> GetAllUsers();

        User GetById(int id);

        void Create(User entity);

        void Update(User entity);

        void Delete(User entity);

        int autentificacion(string user, string hash);

        int uniconombre(string user);

        int IdbyName(string user);

    }
}
