﻿namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addweather : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        LoginName = c.String(),
                        Password = c.String(),
                        AccessLevel = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        CreateUserId = c.Int(nullable: false),
                        EditDate = c.DateTime(),
                        EditUserId = c.Int(),
                        DeleteDate = c.DateTime(),
                        DeleteUserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WeatherData",
                c => new
                    {
                        Date = c.DateTime(nullable: false),
                        TempOut = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HiTemp = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LowTemp = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OutHum = c.Int(nullable: false),
                        DewPt = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WindSpeed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WindDir = c.String(),
                        WindRun = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HiSpeed = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HiDir = c.String(),
                        WindChill = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HeatIndex = c.Decimal(nullable: false, precision: 18, scale: 2),
                        THWIndex = c.Decimal(nullable: false, precision: 18, scale: 2),
                        THSWIndex = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Bar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Rain = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RainRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SolarRad = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SolarEnergy = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HiSolarRad = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CoolDD = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InTemp = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InHum = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InDew = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InHeat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InEMC = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InAirDensity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ET = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WindSamp = c.Int(nullable: false),
                        WinTx = c.Int(nullable: false),
                        ISSRecept = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ArcInt = c.Int(nullable: false),
                        Id = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        CreateUserId = c.Int(nullable: false),
                        EditDate = c.DateTime(),
                        EditUserId = c.Int(),
                        DeleteDate = c.DateTime(),
                        DeleteUserId = c.Int(),
                    })
                .PrimaryKey(t => t.Date);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.WeatherData");
            DropTable("dbo.User");
        }
    }
}
