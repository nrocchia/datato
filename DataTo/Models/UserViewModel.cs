﻿namespace DataTo.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class UserViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Dato requerido")]
        [Display(Name = "Nombre")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Dato requerido")]
        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Dato requerido")]
        [Display(Name = "Usuario")]
        public string LoginName { get; set; }

        [Required(ErrorMessage = "Dato requerido")]
        [StringLength(50, MinimumLength = 5)]
        public string Password { get; set; }

        [Compare("Password")]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 8)]
        [Required(ErrorMessage = "Dato requerido")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Dato requerido")]
        [Display(Name = "Nivel de Acceso")]
        public int AccessLevel { get; set; }

        public string UsuarioLogeado { get; set; }

        public DateTime CreateDate { get; set; }

        public int CreateUserId { get; set; }

        public DateTime? EditDate { get; set; }

        public int? EditUserId { get; set; }

        public DateTime? DeleteDate { get; set; }

        public int? DeleteUserId { get; set; }

    }
}