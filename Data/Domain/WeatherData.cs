﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data.Domain
{
    public class WeatherData : BaseEntity
    {

        [Key]
        public DateTime Date { get; set; }

        public decimal TempOut { get; set; }

        public decimal HiTemp { get; set; }

        public decimal LowTemp { get; set; }

        public int OutHum { get; set; }

        public decimal DewPt { get; set; }

        public decimal WindSpeed { get; set; }

        public string WindDir { get; set; }

        public decimal WindRun { get; set; }

        public decimal HiSpeed { get; set; }

        public string HiDir { get; set; }

        public decimal WindChill { get; set; }

        public decimal HeatIndex { get; set; }

        public decimal THWIndex { get; set; }

        public decimal THSWIndex { get; set; }

        public decimal Bar { get; set; }

        public decimal Rain { get; set; }

        public decimal RainRate { get; set; }

        public decimal SolarRad { get; set; }

        public decimal SolarEnergy { get; set; }

        public decimal HiSolarRad { get; set; }

        public decimal CoolDD { get; set; }

        public decimal InTemp { get; set; }

        public decimal InHum { get; set; }

        public decimal InDew { get; set; }

        public decimal InHeat { get; set; }

        public decimal InEMC { get; set; }

        public decimal InAirDensity { get; set; }

        public decimal ET { get; set; }

        public int WindSamp { get; set; }

        public int WinTx { get; set; }

        public decimal ISSRecept { get; set; }

        public int ArcInt { get; set; }



    }
}
