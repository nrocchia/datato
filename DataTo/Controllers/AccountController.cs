﻿using DataTo.Models;
using Services.Interfaces;
using Services.Services;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;


namespace DataTo.Controllers
{
    public class AccountController : Controller
    {
        IUserService UserService = new UserService();
        // GET: Account
        public ActionResult Login()
        {
            ViewBag.Message = "Login";
            return View("LoginAdminAccount");
        }

        [HttpPost]
        public ActionResult Login(UserViewModel User)
        {
            if (User.LoginName != null && User.Password != null)
            {
                string Hashpass = Helper.EncodePassword(string.Concat(User.LoginName, User.Password));

                //compara user y hash
                int validacion = UserService.autentificacion(User.LoginName, Hashpass);

                if (validacion == 1)
                {

                    FormsAuthentication.SetAuthCookie(User.LoginName, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.LoginError = "Error de usuario o contraseña";
                    return View("LoginAdminAccount");
                }
            }
            ViewBag.LoginError = "Error de usuario o contraseña";
            return View("LoginAdminAccount");
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //encriptacion de user y pass para comparar con hash guardado en pass en sql
        internal class Helper
        {
            public static string EncodePassword(string originalPassword)
            {
                SHA1 sha1 = new SHA1CryptoServiceProvider();
                byte[] inputBytes = (new UnicodeEncoding()).GetBytes(originalPassword);
                byte[] hash = sha1.ComputeHash(inputBytes);
                return Convert.ToBase64String(hash);
            }
        }
    }
}