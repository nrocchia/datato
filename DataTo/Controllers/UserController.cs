﻿namespace DataTo.Controllers
{
    using System;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using Data.Domain;
    using DataTo.Models;
    using Services.Interfaces;
    using Services.Services;

    // GET: User
    public class UserController : AdminController
    {
        private readonly IUserService serviceUser = new UserService();

        public ActionResult Index()
        {
            var users = this.serviceUser.GetAllUsers();

            var model = users.Select(user => new ListUserViewModel()
            {
                Id = user.Id,
                NameComplete = user.FirstName + " " + user.LastName,
            }).ToList();

            return this.View("Index", model);
        }

        public ActionResult Details(int id)
        {
            var user = this.serviceUser.GetById(id);

            var viewModel = new UserViewModel();

            viewModel.FirstName = user.FirstName;

            viewModel.LastName = user.LastName;

            return this.View("Details", viewModel);
        }

        public ActionResult Create()
        {
            var viewModel = new UserViewModel();

            return this.View("Create", viewModel);
        }

        [HttpPost]
        public ActionResult Create(UserViewModel model)
        {
            int idUser = this.serviceUser.IdbyName(model.UsuarioLogeado);
            if (this.ModelState.IsValid)
            {
                // valida nombre usuario unico
                int duplicado = this.serviceUser.uniconombre(model.LoginName);

                if (duplicado == 1) // usuario repetido
                {
                    this.ViewBag.Error = "Usuario Duplicado";
                }
                else
                {
                    // crea un hash con el usuario y password y lo guarda en password
                    string hashpass = Helper.EncodePassword(string.Concat(model.LoginName, model.Password));

                    var user = this.MapUser(model, idUser, hashpass);

                    this.serviceUser.Create(user);

                    return this.RedirectToAction("Index");
                }
            }

            return this.View(model);
        }

        public ActionResult Edit(int id)
        {
            var user = this.serviceUser.GetById(id);

            UserViewModel viewModel = this.MapUserViowModel(user);

            return this.View("Edit", viewModel);
        }

        [HttpPost]
        public ActionResult Edit(UserViewModel model)
        {
            int idUser = this.serviceUser.IdbyName(model.UsuarioLogeado);
            if (this.ModelState.IsValid)
            {
                model.LoginName.ToLower();

                // crea un hash con el usuario y password y lo guarda en password
                string hashpass = Helper.EncodePassword(string.Concat(model.LoginName, model.Password));

                User user = this.MapUser(model, idUser, hashpass);

                this.serviceUser.Update(user);

                return this.RedirectToAction("Index");
            }

            return this.View(model);
        }

        private User MapUser(UserViewModel model, int idUser, string hashpass)
        {
            User response = new User()
            {
                Id = model.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Password = hashpass,
                EditDate = DateTime.Now,
                EditUserId = idUser,
                LoginName = model.LoginName,
                AccessLevel = model.AccessLevel,
                CreateDate = model.CreateDate,
                CreateUserId = model.CreateUserId,
            };

            return response;
        }

        private UserViewModel MapUserViowModel(User user)
        {
            UserViewModel response = new UserViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                LoginName = user.LoginName,
                AccessLevel = user.AccessLevel,
                CreateDate = user.CreateDate,
                CreateUserId = user.CreateUserId,
            };

            return response;
        }

        // encriptacion de la password
        internal class Helper
        {
            public static string EncodePassword(string originalPassword)
            {
                SHA1 sha1 = new SHA1CryptoServiceProvider();
                byte[] inputBytes = new UnicodeEncoding().GetBytes(originalPassword);
                byte[] hash = sha1.ComputeHash(inputBytes);
                return Convert.ToBase64String(hash);
            }
        }

        /// <summary>
        /// Obtiene el id del usuario logueado
        /// </summary>
        /// <returns></returns>
        private int GetIdUserLog()
        {
            string logUser = System.Web.HttpContext.Current.User.Identity.Name.ToString();
            int idlogUser = this.serviceUser.IdbyName(logUser);

            return idlogUser;
        }
    }
}
