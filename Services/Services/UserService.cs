﻿using Data;
using Data.Domain;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Services.Services
{
    public class UserService : IUserService
    {
        EntityContext db = new EntityContext();

        public List<User> GetAllUsers()
        {
            return db.Users.Where(i => i.DeleteDate == null).ToList();
        }

        public User GetById(int id)
        {
            return db.Users.FirstOrDefault(User => User.Id == id);
        }

        public void Create(User entity)
        {
            entity.CreateDate = DateTime.Now;
            db.Entry(entity).State = EntityState.Added;
            db.SaveChanges();
        }

        public void Update(User entity)
        {
            entity.EditDate = DateTime.Now;
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(User entity)
        {
            entity.DeleteDate = DateTime.Now;
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }

        public int autentificacion(string stringuser, string hash)
        {
            int validacion = 0;

            var data = (from User in db.Users
                        where User.LoginName == stringuser &&
                        User.Password == hash
                        select User).FirstOrDefault();
            if (data != null)
            {
                validacion = 1;
            }
            return validacion;
        }

        public int uniconombre(string user)
        {
            int duplicado = 0;
            var data = (from User in db.Users
                        where User.LoginName == user
                        select User).FirstOrDefault();
            if (data != null)
            {
                duplicado = 1;
            }
            return duplicado;
        }

        public int IdbyName(string user)
        {
            int Id = (from User in db.Users
                      where User.LoginName == user
                      select User.Id).FirstOrDefault();
            return Id;
        }

    }
}