﻿namespace Data.Domain
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string LoginName { get; set; }

        public string Password { get; set; }

        public int AccessLevel { get; set; }
    }
}
