﻿using System.Data.Entity;
using Data.Domain;

namespace Data
{
    public class EntityContext : DbContext
    {
        public EntityContext() : base()
        {

        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Configuracion de tabla para Users
            var userConfiguration = modelBuilder.Entity<User>();
            userConfiguration.ToTable("User");

            var weatherDataConfiguration = modelBuilder.Entity<WeatherData>();
            weatherDataConfiguration.ToTable("WeatherData");

            base.OnModelCreating(modelBuilder);
        }
    }
}
